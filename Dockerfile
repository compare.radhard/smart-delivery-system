FROM python:3.8

WORKDIR /app
# ENV VIRTUAL_ENV=/env
# RUN python -m venv $VIRTUAL_ENV
# RUN . /env/bin/activate

# install Dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

# run Application
COPY . .
CMD ["python3", "./app.py", "-e", "production"]

