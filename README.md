# Problem Statement: Smart way to take Decision as Defective/Safe Delivery

## Existing Problem

- Now a days everyone is going to order from Online store instead of shopping from offline  store. Why? 
- Because There is no more Middle man between Factory outlet to Customer, so customer gets the online product at cheaper cost compare to offline store.
- However, day by day increase in online orders then there also be the Cheating Case/Fraud Case either from Delivery Boy Side or from customer side and that is going to be Challenging in upcoming decades.

## Solution

- To resolving this most challenging issue, I came up with one scalable WebApp and WebAPi Solution that will helps for Companies (Flipkart, amazon, etc.) to identify whether this delivery is Safe OR Defective by using this great solution.

- [System Diagram](https://gitlab.com/compare.radhard/smart-delivery-system/-/blob/main/System_Diagram.png)

- **Main Brain**: Deep Learning Technology
- **Tech Stack Used**: Python, Flask, Tensorflow, Keras, Docker
- **Model Accuracy**: 98.67% on 1.2K Training Dataset

## Setup this App in your System

- Clone this repo by clicking on this [link](https://gitlab.com/compare.radhard/smart-delivery-system.git)
- goto Base Directory and open CMD/VS Code
- Run this command: `pip install -r requirements.txt`
- Run the final Application: `python app.py`
- Hurray! Now this App resources steup in your machine.


# Testing

## Tetsting WebAPP
- Open the `http://localhost:5000`
- Upload any open/close Box image and submit and yah! you will get 98.6% of correct result.

## Testing WebAPI
- Open PostMan OR any endpoint testing app
- add url `http://localhost:9000/predict` with `POST` method.
- add one property for file uploading with Key value as `image`
- Hurray! done you just have to click on send request and you will get correct Message output.


# Future Goal
- Currently have build the prototype for testing on either WebAPP or WebAPI.
- Future Goal is to Integrate this system with the Smart Phone which has already provided by respective organization.

# Made By
[Mohit Radadiya](https://www.linkedin.com/in/mohit-radadiya-55b7a7149/)

# Project status
We have build the successfully working prototype for resolving this real world Problem.

