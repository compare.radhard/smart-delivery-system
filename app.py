import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import numpy as np

from keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model
from waitress import serve

# flask
from flask import Flask, request, render_template, jsonify, redirect, url_for

import warnings
warnings.filterwarnings("ignore")

# Define a flask app
app = Flask(__name__)

# Load your trained model
MODEL_PATH = './model/model.h5'
model = load_model(MODEL_PATH)
print("Model Loading Successful!")


# Prediction Function
def output(image):
    icd = {0: 'close', 1: 'open'}
    img=load_img(image,target_size=(224,224,3))
    img=img_to_array(img)
    img=img/255
    img=np.expand_dims(img,[0])
    predict_x=model.predict(img)
    answer = np.argmax(predict_x,axis=1)
    probability=round(predict_x[0][answer[0]]*100, 2)
    lbl=icd[answer[0]]
    return probability, lbl


# Home Page
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/result', methods=['GET','POST'])
def result():
    try:
        file_n = "./static/upload/image.jpg"
        message = ""
        if(request.method == 'POST'):
            files = request.files['file']
            files.save(file_n)
            probability, lbl = output(file_n)
            return render_template('index.html', file_img=file_n, probability=probability, label=lbl)
        if(request.method == 'GET'):
            return redirect(url_for('index'))
        else:
            message = "Webpage does not exist"
            return {"message": message}, 404
    except Exception as e:
        msg = "Sorry! we could not get correct input file. Try Again Later!"
        return render_template('index.html', err_message=msg)



@app.route('/predict', methods=['POST'])
def predict():
    try:
        files = request.files['image']
        file_n = "./static/upload/image.jpg"            
        files.save(file_n)
        probability, lbl = output(file_n)
        message = "{0} % chances are there that the Box Is {1}".format(probability, lbl)
        return jsonify({"Message": message})
    except Exception as e:
        msg = "Sorry! we could not get correct input file. Try Again Later!"
        return jsonify({"Message": msg})


if __name__ == '__main__':
     serve(app, host="0.0.0.0", port=8080)
    # port = int(os.environ.get('PORT', 5000))
    # app.run(host="0.0.0.0", port=5000)
    #uvicorn.run(app, port="8080", host="0.0.0.0")
